package com.otus.spring.book.batch;


import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

import com.mongodb.MongoClient;
import com.otus.spring.book.batch.config.MongoConfigurationProperties;


@TestConfiguration
@Import(EmbeddedMongoAutoConfiguration.class)
@AutoConfigureAfter(EmbeddedMongoAutoConfiguration.class)
public class SpringBatchTestConfiguration
{
    @Bean
    @DependsOn("embeddedMongoServer")
    public MongoClient mongoClient(
        MongoConfigurationProperties mongoConfigurationProperties)
    {
        return new MongoClient(mongoConfigurationProperties.getHost(),
            mongoConfigurationProperties.getPort());
    }

    @Bean
    public JobLauncherTestUtils jobLauncherTestUtils()
    {
        return new JobLauncherTestUtils();
    }
}
