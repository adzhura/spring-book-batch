package com.otus.spring.book.batch;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBatchTestConfiguration.class)
public class SpringBatchTest
{
    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Test
    public void jobTest()
    {
        try
        {
            JobExecution jobExecution = jobLauncherTestUtils.launchJob();
            assertThat(jobExecution.getExitStatus())
                .isEqualTo(ExitStatus.COMPLETED);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            assertThat(e).isNull();
        }
    }
}
