DROP TABLE IF EXISTS COMMENTS;
DROP TABLE IF EXISTS BOOKS;
DROP TABLE IF EXISTS GENRES;
DROP TABLE IF EXISTS AUTHORS;

CREATE TABLE GENRES(
  GNR_ID INT NOT NULL auto_increment PRIMARY KEY, 
  NAME VARCHAR(255) NOT NULL
);

CREATE TABLE AUTHORS(
  AUT_ID INT NOT NULL auto_increment PRIMARY KEY,
  FIRST_NAME VARCHAR(255) NOT NULL, 
  LAST_NAME VARCHAR(255) NOT NULL
);

CREATE TABLE BOOKS(
  BK_ID INT NOT NULL auto_increment PRIMARY KEY, 
  GNR_GNR_ID INT NULL, 
  AUT_AUT_ID INT NOT NULL, 
  TITLE VARCHAR(255) NOT NULL, 
  FOREIGN KEY(GNR_GNR_ID) REFERENCES GENRES(GNR_ID),
  FOREIGN KEY(AUT_AUT_ID) REFERENCES AUTHORS(AUT_ID)
);

CREATE TABLE COMMENTS(
  CMT_ID INT NOT NULL auto_increment PRIMARY KEY, 
  BK_BK_ID INT NOT NULL, 
  USER_NAME VARCHAR(255) NOT NULL, 
  TEXT VARCHAR(255) NOT NULL,
  FOREIGN KEY(BK_BK_ID) REFERENCES BOOKS(BK_ID)
);