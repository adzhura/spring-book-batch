insert into GENRES (gnr_id, name) values (1, 'Юмор');
insert into GENRES (gnr_id, name) values (2, 'Детектив');

insert into AUTHORS (aut_id, first_name, last_name) values (1, 'Иван', 'Иванов');
insert into AUTHORS (aut_id, first_name, last_name) values (2, 'Петр', 'Петров');

insert into BOOKS (bk_id, title, aut_aut_id, gnr_gnr_id) values (1, 'Гарри Поттер', 1, 1);
insert into BOOKS (bk_id, title, aut_aut_id, gnr_gnr_id) values (2, 'Властелин колец', 1, 2);
insert into BOOKS (bk_id, title, aut_aut_id, gnr_gnr_id) values (3, '12 стульев', 2, 1);
insert into BOOKS (bk_id, title, aut_aut_id, gnr_gnr_id) values (4, 'Эркюль Пуаро', 2, 2);


insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (1, 1, 'user1', 'good 1');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (2, 1, 'user2', 'norm 1');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (3, 1, 'user3', 'bad 1');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (4, 2, 'user1', 'good 2');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (5, 2, 'user2', 'norm 2');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (6, 2, 'user3', 'bad 2');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (7, 3, 'user1', 'good 3');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (8, 3, 'user2', 'norm 3');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (9, 3, 'user3', 'bad 3');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (10, 4, 'user1', 'good 4');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (11, 4, 'user2', 'norm 4');
insert into COMMENTS (cmt_id, bk_bk_id, user_name, text) values (12, 4, 'user3', 'bad 4');