package com.otus.spring.book.batch.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import com.mongodb.MongoClient;


@Configuration
public class MongoConfiguration extends AbstractMongoConfiguration
{
    private final MongoConfigurationProperties mongoConfigurationProperties;

    @Autowired
    public MongoConfiguration(
        MongoConfigurationProperties mongoConfigurationProperties)
    {
        this.mongoConfigurationProperties = mongoConfigurationProperties;
    }

    @Bean
    @Override
    public MongoClient mongoClient()
    {
        return new MongoClient(mongoConfigurationProperties.getHost(),
            mongoConfigurationProperties.getPort());
    }

    @Override
    protected String getDatabaseName()
    {
        return mongoConfigurationProperties.getDatabase();
    }
}
