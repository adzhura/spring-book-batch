package com.otus.spring.book.batch.domain.h2;


import com.otus.spring.book.batch.domain.HasId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author implements HasId<Integer>
{
    private Integer id;
    private String firstName;
    private String lastName;
}
