package com.otus.spring.book.batch.service;


import java.util.Optional;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


@Service
public class MongoCleaner implements Tasklet, StepExecutionListener
{
    public static final String CLEAN = "clean";

    private final String mongoCollection;
    private final MongoOperations mongoOperations;

    private boolean isClean;

    @Autowired
    public MongoCleaner(
        @Value("#{mongoConfigurationProperties.getDatabase()}") String mongoCollection,
        MongoOperations mongoOperations)
    {
        this.mongoCollection = mongoCollection;
        this.mongoOperations = mongoOperations;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution,
        ChunkContext chunkContext) throws Exception
    {
        if (isClean)
        {
            mongoOperations.remove(new Query(), mongoCollection);
        }

        return RepeatStatus.FINISHED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution)
    {
        String cleanParam = stepExecution.getJobParameters().getString(CLEAN);

        isClean = Optional.ofNullable(cleanParam).map(Boolean::valueOf)
            .orElse(false);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution)
    {
        return stepExecution.getExitStatus();
    }
}
