package com.otus.spring.book.batch.service;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.otus.spring.book.batch.domain.h2.Author;


@Component
public class AuthorRowMapper implements RowMapper<Author>
{
    @Override
    public Author mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Author(rs.getInt(1), rs.getString(2), rs.getString(3));
    }
}
