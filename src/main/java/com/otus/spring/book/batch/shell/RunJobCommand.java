package com.otus.spring.book.batch.shell;


import java.sql.SQLException;
import java.util.Date;

import org.h2.tools.Console;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import com.otus.spring.book.batch.service.MongoCleaner;


@ShellComponent
public class RunJobCommand
{
    private final JobLauncher jobLauncher;
    private final Job libraryJob;

    private Console h2Console;

    @Autowired
    public RunJobCommand(JobLauncher jobLauncher, Job libraryJob)
    {
        this.jobLauncher = jobLauncher;
        this.libraryJob = libraryJob;
    }

    @ShellMethod(value = "Run job")
    public String runJob(@ShellOption(defaultValue = "none") String clean)
    {
        String string = "runJob " + clean + "\n" + jobLauncher + "\n"
            + libraryJob;

        String isClean = Boolean
            .toString(MongoCleaner.CLEAN.equalsIgnoreCase(clean));

        try
        {
            JobExecution execution = jobLauncher.run(libraryJob,
                new JobParametersBuilder()
                    .addString(MongoCleaner.CLEAN, isClean)
                    .addDate("date", new Date()).toJobParameters());

            return string + "\n" + execution.getExitStatus();
        }
        catch (JobExecutionAlreadyRunningException | JobRestartException
                | JobInstanceAlreadyCompleteException
                | JobParametersInvalidException e)
        {
            return string + "\n" + e;
        }
    }

    @ShellMethod("H2 Console run")
    public String h2Run()
    {
        try
        {
            if (h2Console == null)
            {
                h2Console = new Console();
            }

            h2Console.runTool(new String[0]);

            return "H2 Console";
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    @ShellMethod("H2 Console close")
    public String h2Close()
    {
        if (h2Console != null)
        {
            h2Console.shutdown();
        }

        return "H2 Console";
    }
}
