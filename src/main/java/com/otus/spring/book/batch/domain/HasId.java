package com.otus.spring.book.batch.domain;


public interface HasId<T>
{
    T getId();

    void setId(T id);
}
