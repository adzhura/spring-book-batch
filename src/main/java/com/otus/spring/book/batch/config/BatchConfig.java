package com.otus.spring.book.batch.config;


import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.listener.ExecutionContextPromotionListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.builder.MongoItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.jdbc.core.RowMapper;

import com.otus.spring.book.batch.domain.h2.Author;
import com.otus.spring.book.batch.domain.h2.Book;
import com.otus.spring.book.batch.domain.h2.Comment;
import com.otus.spring.book.batch.domain.h2.Genre;
import com.otus.spring.book.batch.domain.mongo.MongoAuthor;
import com.otus.spring.book.batch.domain.mongo.MongoBook;
import com.otus.spring.book.batch.domain.mongo.MongoGenre;


@Configuration
@EnableBatchProcessing
@ImportResource("classpath:batchConfig.xml")
public class BatchConfig extends DefaultBatchConfigurer
{
    @Autowired
    private DataSource dataSource;

    @Bean
    public ItemReader<Genre> h2GenreReader(RowMapper<Genre> rowMapper)
    {
        return new JdbcCursorItemReaderBuilder<Genre>().name("genreReaderH2")
            .dataSource(dataSource).sql("select GNR_ID, NAME from GENRES")
            .rowMapper(rowMapper).build();
    }

    @Bean
    public ItemWriter<MongoGenre> mongoGenreWriter(
        @Value("#{mongoConfigurationProperties.getDatabase()}") String mongoCollection,
        MongoOperations mongoOperations)
    {
        return new MongoItemWriterBuilder<MongoGenre>()
            .collection(mongoCollection).template(mongoOperations).build();
    }

    @Bean
    public ItemWriter<MongoAuthor> mongoAuthorWriter(
        @Value("#{mongoConfigurationProperties.getDatabase()}") String mongoCollection,
        MongoOperations mongoOperations)
    {
        return new MongoItemWriterBuilder<MongoAuthor>()
            .collection(mongoCollection).template(mongoOperations).build();
    }

    @Bean
    public ItemWriter<MongoBook> mongoBookWriter(
        @Value("#{mongoConfigurationProperties.getDatabase()}") String mongoCollection,
        MongoOperations mongoOperations)
    {
        return new MongoItemWriterBuilder<MongoBook>()
            .collection(mongoCollection).template(mongoOperations).build();
    }

    @Bean
    public ItemReader<Author> h2AuthorReader(RowMapper<Author> rowMapper)
    {
        return new JdbcCursorItemReaderBuilder<Author>().name("authorReaderH2")
            .dataSource(dataSource)
            .sql("select aut_id, first_name, last_name from AUTHORS")
            .rowMapper(rowMapper).build();
    }

    @Bean
    public ItemReader<Book> h2BookReader(RowMapper<Book> rowMapper)
    {
        return new JdbcCursorItemReaderBuilder<Book>().name("bookReaderH2")
            .dataSource(dataSource)
            .sql("select bk_id, title, gnr_gnr_id, aut_aut_id from BOOKS")
            .rowMapper(rowMapper).build();
    }

    @Bean
    public ItemReader<Comment> h2CommentReader(RowMapper<Comment> rowMapper)
    {
        return new JdbcCursorItemReaderBuilder<Comment>()
            .name("commentsReaderH2").dataSource(dataSource)
            .sql("select cmt_id, bk_bk_id, user_name, text from COMMENTS")
            .rowMapper(rowMapper).build();
    }

    @Bean
    public ExecutionContextPromotionListener promotionListener()
    {
        ExecutionContextPromotionListener listener = new ExecutionContextPromotionListener();

        listener.setKeys(new String[] {PromotionKey.GENRE_KEY_MAP,
            PromotionKey.AUTHOR_KEY_MAP, PromotionKey.COMMENTS});

        return listener;
    }
}
