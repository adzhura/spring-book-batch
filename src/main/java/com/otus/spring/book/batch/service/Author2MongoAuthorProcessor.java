package com.otus.spring.book.batch.service;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

import com.otus.spring.book.batch.config.PromotionKey;
import com.otus.spring.book.batch.domain.h2.Author;
import com.otus.spring.book.batch.domain.mongo.MongoAuthor;


@Service
public class Author2MongoAuthorProcessor
        implements ItemProcessor<Author, MongoAuthor>
{
    private Map<Integer, String> keyMap;

    @Override
    public MongoAuthor process(Author author) throws Exception
    {
        String uuid = UUID.randomUUID().toString();
        keyMap.put(author.getId(), uuid);
        return new MongoAuthor(uuid, author.getFirstName(),
            author.getLastName());
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution)
    {
        keyMap = new HashMap<>();
        stepExecution.getExecutionContext().put(PromotionKey.AUTHOR_KEY_MAP,
            keyMap);
    }
}
