package com.otus.spring.book.batch.service;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

import com.otus.spring.book.batch.config.PromotionKey;
import com.otus.spring.book.batch.domain.h2.Genre;
import com.otus.spring.book.batch.domain.mongo.MongoGenre;


@Service
public class Genre2MongoGenreProcessor
        implements ItemProcessor<Genre, MongoGenre>
{
    private Map<Integer, String> keyMap;

    @Override
    public MongoGenre process(Genre genre) throws Exception
    {
        String uuid = UUID.randomUUID().toString();
        keyMap.put(genre.getId(), uuid);
        return new MongoGenre(uuid, genre.getName());
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution)
    {
        keyMap = new HashMap<>();
        stepExecution.getExecutionContext().put(PromotionKey.GENRE_KEY_MAP,
            keyMap);
    }
}
