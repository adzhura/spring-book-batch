package com.otus.spring.book.batch.domain;


public interface NamedObject<T> extends HasId<T>
{
    String getName();
}
