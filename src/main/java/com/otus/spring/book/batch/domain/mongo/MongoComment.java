package com.otus.spring.book.batch.domain.mongo;


import org.springframework.data.mongodb.core.mapping.Field;

import com.otus.spring.book.batch.domain.HasId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "userName", "text"})
@EqualsAndHashCode(of = {"id", "userName", "text"})
public class MongoComment implements HasId<String>
{
    @Field("cmt_id")
    private String id;

    @Field("user_name")
    private String userName;

    private String text;
}
