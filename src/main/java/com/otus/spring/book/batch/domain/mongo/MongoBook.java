package com.otus.spring.book.batch.domain.mongo;


import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.otus.spring.book.batch.domain.NamedObject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@TypeAlias("book")
@ToString(of = {"id", "name", "genreId", "authorId"})
@EqualsAndHashCode(of = {"id", "name", "genreId", "authorId"})
@Document(collection = "#{mongoConfigurationProperties.getDatabase()}")
public class MongoBook implements NamedObject<String>
{
    @Id
    private String id;

    @Field("title")
    private String name;

    private String genreId;

    private String authorId;

    private List<MongoComment> comments;
}
