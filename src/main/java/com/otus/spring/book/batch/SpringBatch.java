package com.otus.spring.book.batch;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class SpringBatch
{
    public static void main(String[] args) throws Exception
    {
        try (ConfigurableApplicationContext ctx = SpringApplication
            .run(SpringBatch.class))
        {
        }
    }
}
