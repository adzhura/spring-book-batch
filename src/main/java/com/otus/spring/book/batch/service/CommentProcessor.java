package com.otus.spring.book.batch.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

import com.otus.spring.book.batch.config.PromotionKey;
import com.otus.spring.book.batch.domain.h2.Comment;


@Service
public class CommentProcessor implements ItemProcessor<Comment, Comment>
{
    private Map<Integer, List<Comment>> commentMap;

    @Override
    public Comment process(Comment comment) throws Exception
    {
        commentMap.merge(comment.getBookId(), new ArrayList<>(),
            (v1, v2) -> v1 != null ? v1 : v2).add(comment);

        return comment;
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution)
    {
        commentMap = new HashMap<>();
        stepExecution.getExecutionContext().put(PromotionKey.COMMENTS,
            commentMap);
    }
}
