package com.otus.spring.book.batch.domain.h2;


import com.otus.spring.book.batch.domain.NamedObject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book implements NamedObject<Integer>
{
    private Integer id;
    private String name;
    private Integer genreId;
    private Integer authorId;
}
