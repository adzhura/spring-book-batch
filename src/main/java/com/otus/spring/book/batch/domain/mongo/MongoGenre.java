package com.otus.spring.book.batch.domain.mongo;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import com.otus.spring.book.batch.domain.NamedObject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@TypeAlias("genre")
@AllArgsConstructor
@ToString(of = {"id", "name"})
@EqualsAndHashCode(of = {"id", "name"})
@Document(collection = "#{mongoConfigurationProperties.getDatabase()}")
public class MongoGenre implements NamedObject<String>
{
    @Id
    private String id;

    private String name;

    public MongoGenre()
    {
    }

    public MongoGenre(String name)
    {
        this.name = name;
    }
}
