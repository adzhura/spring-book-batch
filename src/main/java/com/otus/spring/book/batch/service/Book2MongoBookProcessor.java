package com.otus.spring.book.batch.service;


import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.otus.spring.book.batch.config.PromotionKey;
import com.otus.spring.book.batch.domain.h2.Book;
import com.otus.spring.book.batch.domain.h2.Comment;
import com.otus.spring.book.batch.domain.mongo.MongoBook;
import com.otus.spring.book.batch.domain.mongo.MongoComment;


@Service
public class Book2MongoBookProcessor implements ItemProcessor<Book, MongoBook>
{
    private Map<Integer, List<Comment>> commentMap = new HashMap<>();
    private Map<Integer, String> genreKeyMap = new HashMap<>();
    private Map<Integer, String> authorKeyMap = new HashMap<>();

    private final Comment2MongoCommentProcessor comment2MongoCommentProcessor;

    @Autowired
    public Book2MongoBookProcessor(
        Comment2MongoCommentProcessor comment2MongoCommentProcessor)
    {
        this.comment2MongoCommentProcessor = comment2MongoCommentProcessor;
    }

    @Override
    public MongoBook process(Book book) throws Exception
    {
        List<MongoComment> comments = commentMap
            .getOrDefault(book.getId(), Collections.emptyList()).stream()
            .map(comment2MongoCommentProcessor::process)
            .collect(Collectors.toList());

        return new MongoBook(book.getId().toString(), book.getName(),
            genreKeyMap.getOrDefault(book.getGenreId(),
                book.getGenreId().toString()),
            authorKeyMap.getOrDefault(book.getAuthorId(),
                book.getAuthorId().toString()),
            comments);
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution)
    {
        genreKeyMap = getParam(stepExecution, PromotionKey.GENRE_KEY_MAP,
            new HashMap<>());
        authorKeyMap = getParam(stepExecution, PromotionKey.AUTHOR_KEY_MAP,
            new HashMap<>());
        commentMap = getParam(stepExecution, PromotionKey.COMMENTS,
            new HashMap<>());
    }

    @SuppressWarnings("unchecked")
    private <K, V> Map<K, V> getParam(StepExecution stepExecution, String key,
        Map<K, V> defaultValue)
    {
        Object param = stepExecution.getJobExecution().getExecutionContext()
            .get(key);

        if (param instanceof Map)
        {
            return (Map<K, V>) param;
        }

        return defaultValue;
    }
}
