package com.otus.spring.book.batch.service;


import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

import com.otus.spring.book.batch.domain.h2.Comment;
import com.otus.spring.book.batch.domain.mongo.MongoComment;


@Service
public class Comment2MongoCommentProcessor
        implements ItemProcessor<Comment, MongoComment>
{
    @Override
    public MongoComment process(Comment comment)
    {
        return new MongoComment(comment.getId().toString(),
            comment.getUserName(), comment.getText());
    }
}
