package com.otus.spring.book.batch.config;


import java.util.Optional;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.mongodb.ServerAddress;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Component
@ConfigurationProperties("spring.data.mongodb")
public class MongoConfigurationProperties
{
    private String host;
    private Integer port;
    private String database;

    public String getHost()
    {
        return Optional.ofNullable(host).orElse(ServerAddress.defaultHost());
    }

    public int getPort()
    {
        return Optional.ofNullable(port).orElse(ServerAddress.defaultPort());
    }
}