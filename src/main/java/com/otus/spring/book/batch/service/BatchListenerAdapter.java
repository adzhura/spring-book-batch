package com.otus.spring.book.batch.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.stereotype.Component;


@Component
public class BatchListenerAdapter
        implements JobExecutionListener, StepExecutionListener, ChunkListener
{
    private final Logger logger = LoggerFactory.getLogger("Batch");

    public ChunkListener chunkListener()
    {
        return this;
    }

    @Override
    public void beforeChunk(ChunkContext chunkContext)
    {
        logger.info("Begin chunk: " + chunkContext);
    }

    @Override
    public void afterChunk(ChunkContext chunkContext)
    {
        logger.info("End chunk: " + chunkContext);
    }

    @Override
    public void afterChunkError(ChunkContext chunkContext)
    {
        logger.info("Chunk error: " + chunkContext);
    }

    public JobExecutionListener jobExecutionListener()
    {
        return this;
    }

    @Override
    public void beforeJob(JobExecution jobExecution)
    {
        logger.info("Begin job: " + jobExecution);
    }

    @Override
    public void afterJob(JobExecution jobExecution)
    {
        logger.info("End job " + jobExecution);
    }

    public StepExecutionListener stepExecutionListener()
    {
        return this;
    }

    @Override
    public void beforeStep(StepExecution stepExecution)
    {
        logger.info("Begin step: " + toString(stepExecution));
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution)
    {
        logger.info("End step: " + toString(stepExecution));

        return stepExecution.getExitStatus();
    }

    private String toString(StepExecution stepExecution)
    {
        return String.format("id = %1$d, version = %2$d, stepName = %3$s",
            stepExecution.getId(), stepExecution.getVersion(),
            stepExecution.getStepName());
    }
}
