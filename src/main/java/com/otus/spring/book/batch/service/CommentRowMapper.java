package com.otus.spring.book.batch.service;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.otus.spring.book.batch.domain.h2.Comment;


@Component
public class CommentRowMapper implements RowMapper<Comment>
{
    @Override
    public Comment mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Comment(rs.getInt(1), rs.getInt(2), rs.getString(3),
            rs.getString(4));
    }
}
