package com.otus.spring.book.batch.service;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.otus.spring.book.batch.domain.h2.Book;


@Component
public class BookRowMapper implements RowMapper<Book>
{
    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Book(rs.getInt(1), rs.getString(2), rs.getInt(3),
            rs.getInt(4));
    }
}
