package com.otus.spring.book.batch.config;


public interface PromotionKey
{
    String COMMENTS = "comments";
    String GENRE_KEY_MAP = "genreKeyMap";
    String AUTHOR_KEY_MAP = "authorKeyMap";
}
