package com.otus.spring.book.batch.domain.mongo;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.otus.spring.book.batch.domain.HasId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@AllArgsConstructor
@TypeAlias("author")
@ToString(of = {"id", "firstName", "lastName"})
@EqualsAndHashCode(of = {"id", "firstName", "lastName"})
@Document(collection = "#{mongoConfigurationProperties.getDatabase()}")
public class MongoAuthor implements HasId<String>
{
    @Id
    private String id;

    @Field("first_name")
    private String firstName;

    @Field("last_name")
    private String lastName;
}
