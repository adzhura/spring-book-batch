package com.otus.spring.book.batch.service;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.otus.spring.book.batch.domain.h2.Genre;


@Component
public class GenreRowMapper implements RowMapper<Genre>
{
    @Override
    public Genre mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Genre(rs.getInt(1), rs.getString(2));
    }
}
